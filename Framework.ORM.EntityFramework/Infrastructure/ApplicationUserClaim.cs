﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Framework.ORM.Infrastructure
{
    public class ApplicationUserClaim : IdentityUserClaim<Guid> { }

    public class ApplicationUserClaimEntityConfig : EntityTypeConfiguration<ApplicationUserClaim>
    {
        public ApplicationUserClaimEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserClaim", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.UserId,
                    p.ClaimValue,
                    p.ClaimType
                });
            }).HasKey(c => c.Id);

        }
    }
}
