﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Framework.ORM.Infrastructure
{
    using System.Data.Entity.ModelConfiguration;
    using Interface;
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IApplicationUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id { get; set; }

        [Required]
        public DateTime JoinDate { get; set; }

    }

    public class ApplicationUserEntityConfig : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("User", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.AccessFailedCount,
                    p.Email,
                    p.EmailConfirmed,
                    p.PasswordHash,
                    p.PhoneNumber,
                    p.PhoneNumberConfirmed,
                    p.TwoFactorEnabled,
                    p.SecurityStamp,
                    p.LockoutEnabled,
                    p.LockoutEndDateUtc,
                    p.UserName,
                    p.JoinDate
                });
            }).HasKey(c => c.Id);
            this.HasMany(c => c.Logins).WithOptional().HasForeignKey(c => c.UserId);
            this.HasMany(c => c.Claims).WithOptional().HasForeignKey(c => c.UserId);
            this.HasMany(c => c.Roles).WithRequired().HasForeignKey(c => c.UserId);

        }
    }
}
