﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Framework.ORM.Infrastructure
{
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid();
        }
    }

    public class ApplicationRoleEntityConfig : EntityTypeConfiguration<ApplicationRole>
    {
        public ApplicationRoleEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("Role", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.Name
                });
            }).HasKey(p => p.Id);

            this.HasMany(c => c.Users).WithRequired().HasForeignKey(c => c.RoleId);
        }
    }
}
