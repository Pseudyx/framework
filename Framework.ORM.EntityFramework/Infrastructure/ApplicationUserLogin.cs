﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Framework.ORM.Infrastructure
{
    public class ApplicationUserLogin : IdentityUserLogin<Guid> { }

    public class ApplicationUserLoginEntityConfig : EntityTypeConfiguration<ApplicationUserLogin>
    {
        public ApplicationUserLoginEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserLogin", "Core");
                c.Properties(p => new
                {
                    p.UserId,
                    p.LoginProvider,
                    p.ProviderKey
                });
            }).HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });

        }
    }
}
