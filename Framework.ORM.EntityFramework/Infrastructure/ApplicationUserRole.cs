﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Framework.ORM.Infrastructure
{
    public class ApplicationUserRole : IdentityUserRole<Guid> { }

    public class ApplicationUserRoleEntityConfig : EntityTypeConfiguration<ApplicationUserRole>
    {
        public ApplicationUserRoleEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserRole", "Core");
                c.Properties(p => new
                {
                    p.UserId,
                    p.RoleId
                });
            })
            .HasKey(c => new { c.UserId, c.RoleId });
        }
    }
}
