﻿using System.IO;

namespace Framework.Interface
{
    public interface IEmail
    {
        void AddHTML(string htmlText);
        void AddText(string text);
        void AddAttachment(Stream stream, string fileName, string mediaType);
        void Send();
    }
}
