﻿using System;

namespace Framework.ORM.Infrastructure
{
    using Tribal.AspNet.Identity.PetaPoco;
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid();
        }
    }
}
