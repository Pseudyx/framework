﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Tribal.Data.Entity;

namespace Framework.ORM.Service
{
    using Repository;
    public class EntityService
    {
        public readonly EntityDbContext _dbContext;

        public EntityService()
        {

        }

        public EntityService(EntityDbContext entityDbContext)
        {
            _dbContext = entityDbContext;
        }

        public virtual IQueryable<TEntity> GetAll<TEntity>()
        {
            var sql = Sql.Builder.Select("*").From(typeof(TEntity));
            var dbSet = (_dbContext as EntityDbContext).Query<TEntity>(sql);
            return (IQueryable<TEntity>)dbSet;
        }

        public virtual TEntity GetById<TEntity, T>(T id)
        {
            var dbSet = (_dbContext as EntityDbContext).Single<TEntity>(id);
            return (TEntity)dbSet;
        }

        public virtual TEntity GetByApplicationUserId<TEntity, TKey>(TKey userId)
        {
            var sql = Sql.Builder.Select("*").From(typeof(TEntity)).Where("ApplicationUserId == @0", userId);
            var dbSet = (_dbContext as EntityDbContext).Query<TEntity>(sql);
            return ((IQueryable<TEntity>)dbSet).FirstOrDefault();
        }


        public virtual IEnumerable<TEntity> GetEnumerableByApplicationUserId<TEntity, TKey>(TKey userId)
        {
            var sql = Sql.Builder.Select("*").From(typeof(TEntity)).Where("ApplicationUserId == @0", userId);
            var dbSet = (_dbContext as EntityDbContext).Query<TEntity>(sql);
            return dbSet;
        }

    }
}
