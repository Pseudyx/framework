﻿using System;
using Tribal.AspNet.Identity.PetaPoco;

namespace Framework.ORM.Repository
{
    using Infrastructure;
    public class EntityDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public EntityDbContext() : base("Core")
        {
        }

        public EntityDbContext(string connectionString) : base(connectionString)
        {
        }

        public static EntityDbContext Create()
        {
            return new EntityDbContext();
        }
    }
}
