﻿using System;

namespace Framework.ORM.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserClaim : IdentityUserClaim<Guid> { }
}
