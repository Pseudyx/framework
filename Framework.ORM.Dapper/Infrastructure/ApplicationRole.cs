﻿using System;

namespace Framework.ORM.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid();
        }
    }
}