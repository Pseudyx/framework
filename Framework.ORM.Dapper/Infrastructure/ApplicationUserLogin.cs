﻿using System;

namespace Framework.ORM.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserLogin : IdentityUserLogin<Guid> { }
}
