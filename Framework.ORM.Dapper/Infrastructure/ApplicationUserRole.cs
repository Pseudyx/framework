﻿using System;

namespace Framework.ORM.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserRole : IdentityUserRole<Guid> { }
}
